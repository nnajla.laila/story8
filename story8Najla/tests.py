from django.test import TestCase
from django.test import Client
from django.urls import resolve
from . import views

# Create your tests here.
class Story8UnitTest(TestCase):
    def test_books_url_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)
    
    def test_books_page_use_right_function(self):
        function = resolve('/story8/')
        self.assertEqual(function.func, views.index)

    def test_books_page_use_login_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'indexBook.html')
    
    def test_url_data_json_exist(self):
        response = Client().get("/story8/data/")
        self.assertEqual(response.status_code, 200)
        
    def test_data_json_use_right_function(self):
        found = resolve("/story8/data/")
        self.assertEqual(found.func, views.data)



